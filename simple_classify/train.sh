

PD=$HOME/dataspace/graph/econ-mahindas
PREFIX2=semi-synthetic/REGAL-d2-seed1


python main.py --graphdir data/cora/cora --task classify --loss_type mincut --weight_decay_type elem --seed 1 --edge_path ${PD}/graphsage/edges.edgelist.npy --id2idx ${PD}/graphsage/id2idx.json --emb_path ${PD}/graphsage/mincut_emb.npy --num-parts 128
python main.py --graphdir data/cora/cora --task classify --loss_type mincut --weight_decay_type elem --seed 1 --edge_path ${PD}/${PREFIX2}/graphsage/edges.edgelist.npy --id2idx ${PD}/${PREFIX2}/graphsage/id2idx.json --emb_path ${PD}/${PREFIX2}/graphsage/mincut_emb.npy



PD=$HOME/dataspace/graph/douban/

python main.py --graphdir data/cora/cora --task classify --loss_type mincut --weight_decay_type elem --seed 1 --edge_path ${PD}/online/graphsage/edges.edgelist.npy --id2idx ${PD}/online/graphsage/id2idx.json --emb_path ${PD}/online/graphsage/mincut_emb.npy --num-parts 2
python main.py --graphdir data/cora/cora --task classify --loss_type mincut --weight_decay_type elem --seed 1 --edge_path ${PD}/offline/graphsage/edges.edgelist.npy --id2idx ${PD}/offline/graphsage/id2idx.json --emb_path ${PD}/offline/graphsage/mincut_emb.npy --num-parts 2



PD=$HOME/dataspace/graph/bn
PREFIX2=semi-synthetic/REGAL-d2-seed1


python main.py --graphdir data/cora/cora - classify --loss_type mincut --weight_decay_type elem --seed 1 --edge_path ${PD}/graphsage/edges.edgelist.npy --id2idx ${PD}/graphsage/id2idx.json --emb_path ${PD}/graphsage/mincut_emb.npy --num-parts 5 --num_nodes_each 100 --num_cluster 5 --dense_inner 0.5 --dense_outer 0.01 --lambda 0.3 --cluster_name bn1.pkl




python main.py --graphdir data/cora/cora --task classify \
--loss_type mincut --weight_decay_type elem --seed 1 \
--edge_path ${PD}/graphsage/edges.edgelist.npy --id2idx ${PD}/graphsage/id2idx.json \
--emb_path ${PD}/graphsage/mincut_emb.npy \
--num-parts 4 \
--num_nodes_each 100 \
--num_cluster 4 \
--dense_inner 0.7 \
--dense_outer 0.2 \
--lambda 0.99999 \
--emb_model lol \
--cluster_name bn1.pkl \
--epochs 5000 \
--new 


