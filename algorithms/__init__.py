from algorithms.BigAlign import BigAlign
from algorithms.DeepLink import DeepLink
from algorithms.FINAL import FINAL
from algorithms.IONE import IONE
from algorithms.IsoRank import IsoRank
from algorithms.NAWAL import NAWAL
from algorithms.REGAL import REGAL
from algorithms.FAGCN import FAGCN
from algorithms.PALE import PALE
from algorithms.CENALP import CENALP

__all__ = ['BigAlign', 'DeepLink', 'FINAL', 'IONE', 'IsoRank', 'NAWAL', 'REGAL', 'FAGCN', 'PALE', 'CENALP']

